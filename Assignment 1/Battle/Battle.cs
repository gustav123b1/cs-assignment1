﻿using Assignment_1.CharacterRelated;
using Assignment_1.InventoryRelated;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment_1.Battle
{
    public class Battle
    {
        private List<Character> battlers = new List<Character>();
        public bool BattleStarted = false;

        public Battle(Character character1, Character character2)
        {
            battlers.Add(character1);
            battlers.Add(character2);
        }

        public int StartBattle()
        {
            if (BattleStarted) return 0;

            while(AnyBattlerDeadCheck() == false)
            {
                for (int i = 0; i < battlers.Count; i++)
                {
                    Character attacker = battlers[i];
                    Character target = battlers[(i + 1) % 2];
                    Item weapon = attacker.GetWeapon();

                    Console.WriteLine("");
                    Console.WriteLine($"{attacker.Name} attacked {target.Name} with a {weapon.Type}, for {attacker.GetDamage()} damage.");
                    Console.WriteLine($"{target.Name} now has {target.GetHp() - attacker.GetDamage()} hp. {target.GetHp()} - {attacker.GetDamage()} -> {target.GetHp() - attacker.GetDamage()}");
                    Console.WriteLine("");

                    target.DecreaseHp(attacker.GetDamage());
                    if (target.GetHp() <= 0)
                    {
                        BattleOver(attacker, target);
                        break;
                    }
                }
            }

            return 1;
        }

        private void BattleOver(Character winner, Character loser)
        {
            Console.WriteLine("BATTLE OVER");
            Console.WriteLine($"{winner.Name} Won the battle over {loser.Name}");
        }

        private bool AnyBattlerDeadCheck() 
        {
            foreach (Character battler in battlers)
                if (battler.GetHp() <= 0) return true;
            return false; 
        }
    }
}
