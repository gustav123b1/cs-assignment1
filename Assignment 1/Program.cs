﻿// See https://aka.ms/new-console-template for more information
using Assignment_1;
using Assignment_1.Battle;
using Assignment_1.CharacterRelated;
using Assignment_1.InventoryRelated;
using Assignment_1.InventoryRelated.ArmourItem;
using Assignment_1.InventoryRelated.WeaponItem;


// Create ranger
Ranger ranger = new Ranger("Joe Ranger");
Weapon itemToAdd = new Weapon(WeaponType.Bow) 
{ 
    Level = 1, 
    Slot = "Weapon", 
    WeaponAttributes = new WeaponAttributes() { Damage = 5, AttackSpeed = 5 },
    Name="Bow" 
};
Armour leather = new Armour(ArmourType.Leather) { Attributes = new PrimaryAttributes() { Dexterity = 5 } };
ranger.AddItemToInventory(itemToAdd);
ranger.AddItemToInventory(leather);

// Create warrior
Warrior warrior = new Warrior("Joe Warrior");
Weapon warriorAxe = new Weapon(WeaponType.Axe)
{
    Level = 1,
    Slot = "Weapon",
    WeaponAttributes = new WeaponAttributes() { Damage = 15, AttackSpeed = 2 },
    Name = "Axe"
};
Armour warriorArmour = new Armour(ArmourType.Plate) { Attributes = new PrimaryAttributes() { Strength = 5 } };
warrior.AddItemToInventory(warriorAxe);
warrior.AddItemToInventory(warriorArmour);

// Create battle between Ranger and Warrior
Battle battle = new Battle(ranger, warrior);
battle.StartBattle();

