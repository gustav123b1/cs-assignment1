﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment_1.InventoryRelated
{
    public class InventoryException : Exception
    {
        public InventoryException(string message) : base(message)
        {

        }

        public override string Message => "Inventory exception";
    }
}
