﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment_1.InventoryRelated
{
    public class Item
    {
        public bool Empty { get; set; } = true;
        public int Level { get; set; } = 1;
        public string Slot { get; set; } = "unset";
        public string Name { get; set; } = "unset";
        public dynamic Type { get; set; }
        public PrimaryAttributes Attributes { get; set; } = new PrimaryAttributes();
    }
}
