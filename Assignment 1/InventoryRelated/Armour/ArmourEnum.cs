﻿public enum ArmourType
{
    Cloth,
    Leather,
    Mail,
    Plate
}