﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment_1.InventoryRelated.ArmourItem
{
    public class Armour : Item
    {
        public Armour(ArmourType armourType)
        {
            Empty = false;
            Type = armourType;
        }
    }
}
