﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment_1.InventoryRelated.WeaponItem
{
    public class WeaponAttributes
    {
        public double Damage { get; set; } = 0;
        public double AttackSpeed { get; set; } = 0;

        public WeaponAttributes()
        {
            Damage = 0;
            AttackSpeed = 0;
        }
    }
}
