﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment_1.InventoryRelated.WeaponItem
{
    public class Weapon : Item
    {
        public WeaponAttributes WeaponAttributes { get; set; } = new WeaponAttributes();

        public Weapon(WeaponType weaponType)
        {
            Empty = false;
            Type = weaponType;
        }
    }
}
