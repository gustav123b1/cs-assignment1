﻿using Assignment_1.InventoryRelated.WeaponItem;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment_1.InventoryRelated
{
    public class Inventory
    {
        private Dictionary<int, dynamic>? ItemsInventory { get; set; }

        public Inventory()
        {
            InitInventory();
        }

        private void InitInventory()
        {
            int size = 4;

            ItemsInventory = new Dictionary<int, dynamic>();
            for (int i = 0; i < size; i++)
            {
                ItemsInventory.Add(i, new Item());
            }
        }

        public int GetInventorySize()
        {
            return ItemsInventory!.Count;
        }

        public int GetEmptySlot()
        {
            foreach (KeyValuePair<int, dynamic> item in ItemsInventory!)
            {
                if (item.Value.Empty)
                    return item.Key;
            }

            throw new InventoryException("Inventory is full.");
        }

        public int GetWeaponSlot()
        {
            foreach (KeyValuePair<int, dynamic> item in ItemsInventory!)
            {
                if (item.Value.Slot == "Weapon")
                    return item.Key;
            }

            return GetEmptySlot();
        }

        // When index is not provided, get the first empty slot
        public string AddItem(Item itemToAdd)
        {
            int index = itemToAdd.Slot == "Weapon" ? GetWeaponSlot() : GetEmptySlot();
            return AddItemAtIndex(itemToAdd, index);
        }

        public string AddItem(Item itemToAdd, int index)
        {
            return AddItemAtIndex(itemToAdd, index);
        }

        private string AddItemAtIndex(Item itemToAdd, int index)
        {
            try
            {
                Item itemAtIndex = ItemsInventory![index];

                // When replacing an item in the inventory both items must be of same type.
                if (itemAtIndex.Empty == false && itemToAdd.Slot != itemAtIndex.Slot)
                {
                    throw new InventoryException($"Item of type {itemToAdd.Slot} cannot be placed in a {itemAtIndex.Slot} slot.");
                }

                ItemsInventory![index] = itemToAdd;
                return itemToAdd is Weapon ? "New weapon equipped!" : "New armour equipped!";

            }
            catch(InventoryException ex)
            {
                throw new InventoryException(ex.Message);
            }
            catch (KeyNotFoundException ex)
            {
                throw new KeyNotFoundException(ex.Message);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                //throw new InventoryException($"index {index} is out of bounds.");
            }
            return "Something went wrong..";
        }

        public dynamic GetItem(int index)
        {
            try
            {
                var item = ItemsInventory![index];
                return item;
            }
            catch (Exception)
            {
                throw new InventoryException("Could not get item at index " + index + ".");
            }
        }
    }
}
