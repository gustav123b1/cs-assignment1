﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment_1
{
    public class PrimaryAttributes
    {
        public int Strength { get; set; }
        public int Dexterity { get; set; }
        public int Intelligence { get; set; }
        public string Primary { get; set; }

        public PrimaryAttributes()
        {
            Strength = 0;
            Dexterity = 0;
            Intelligence = 0;
        }

        public void SetAll(int str, int dxt, int inte)
        {
            Strength = str;
            Dexterity = dxt;
            Intelligence = inte;
        }

        public int[] GetAll()
        {
            int[] arr = new int[3];
            arr[0] = Strength; arr[1] = Dexterity; arr[2] = Intelligence;
            return arr;
        }

        public void IncreaseAll(int str, int dxt, int inte)
        {
            Strength += str;
            Dexterity += dxt;
            Intelligence += inte;
        }
    }
}
