﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Assignment_1.InventoryRelated;
using Assignment_1.InventoryRelated.ArmourItem;
using Assignment_1.InventoryRelated.WeaponItem;

namespace Assignment_1.CharacterRelated
{
    public class Character
    {
        public PrimaryAttributes attributes = new PrimaryAttributes();
        public PrimaryAttributes basePrimaryAttrubytes = new PrimaryAttributes();
        public PrimaryAttributes statGain = new PrimaryAttributes();
        protected Inventory inventory = new Inventory();
        private int level = 1;
        private double hp = 100;
        public string Name { get; set; } = "unset";
        public List<WeaponType> AllowedWeaponTypes = new List<WeaponType>();
        public List<ArmourType> AllowedArmourTypes = new List<ArmourType>();


        public void LevelUp()
        {
            attributes.Strength = attributes.Strength + statGain.Strength;
            attributes.Dexterity = attributes.Dexterity + statGain.Dexterity;
            attributes.Intelligence = attributes.Intelligence + statGain.Intelligence;

            level++;
        }

        public int GetLevel()
        {
            return level;
        }

        public int AddItemToInventory(Item item)
        {
            if (item.Level > level)
            {
                if (item is Weapon)
                    throw new InvalidWeaponException($"Not able to equip a weapon({item.Level}) with a higher level than the character({level})");
                else
                    throw new InvalidArmorException($"Not able to equip armour({item.Level}) with a higher level than the character({level})");
            }

            if (item is Weapon)
            {
                foreach (WeaponType weaponType in AllowedWeaponTypes)
                {
                    Console.WriteLine($"weaponType: {weaponType}, item.Type: {item.Type}");
                    if (weaponType == item.Type)
                    {
                        inventory.AddItem(item);
                        return 1;
                    }
                }
                throw new InvalidWeaponException($"Not able to equip weapon of type {item.Type}");
            }
            else if (item is Armour)
            {
                foreach (ArmourType armourType in AllowedArmourTypes)
                {
                    if (armourType == item.Type)
                    {
                        inventory.AddItem(item);
                        return 1;
                    }
                }
                throw new InvalidArmorException($"Not able to equip armour of type {item.Type}");
            }

            return -1;
        }

        public Item GetInventoryItem(int index)
        {
            return inventory.GetItem(index);
        }

        public Item GetWeapon()
        {
            int index = inventory.GetWeaponSlot();
            return GetInventoryItem(index);
        }

        public PrimaryAttributes GetTotalAttributes()
        {
            PrimaryAttributes totalAttributes = new PrimaryAttributes();
            totalAttributes.SetAll(attributes.Strength, attributes.Dexterity, attributes.Intelligence);

            for (int i = 0; i < inventory.GetInventorySize(); i++)
            {
                Item item = inventory.GetItem(i);

                if (item is Armour)
                    totalAttributes.IncreaseAll(item.Attributes.Strength, item.Attributes.Dexterity, item.Attributes.Intelligence);
            }

            return totalAttributes;
        }

        public int GetPrimaryAttribute()
        {
            if (attributes.Primary == "Strength")
                return GetTotalAttributes().Strength;
            else if (attributes.Primary == "Dexterity")
                return GetTotalAttributes().Dexterity;
            return GetTotalAttributes().Intelligence;
        }

        public double GetDamage()
        {
            var weapon = inventory.GetItem(inventory.GetWeaponSlot());
            double weaponDPS = weapon is Weapon ? weapon.WeaponAttributes.Damage * weapon.WeaponAttributes.AttackSpeed : 1;
            return weaponDPS * (1 + (GetPrimaryAttribute() / 100));
        }

        public double GetHp()
        {
            return hp;
        }

        public void DecreaseHp(double am)
        {
            hp -= am;
        }
    }
}
