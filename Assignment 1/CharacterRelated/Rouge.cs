﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment_1.CharacterRelated
{
    public class Rouge : Character
    {
        public Rouge(string name)
        {
            Name = name;

            // Init the stats
            basePrimaryAttrubytes.SetAll(2, 6, 1);
            attributes.SetAll(2, 6, 1);
            statGain.SetAll(1, 4, 1);
            attributes.Primary = "Dexterity";

            // Allowed weapon types
            AllowedWeaponTypes.Add(WeaponType.Dagger);
            AllowedWeaponTypes.Add(WeaponType.Sword);

            // Allowed armour types
            AllowedArmourTypes.Add(ArmourType.Leather);
            AllowedArmourTypes.Add(ArmourType.Mail);

        }
    }
}
