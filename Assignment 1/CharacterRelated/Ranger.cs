﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment_1.CharacterRelated
{
    public class Ranger : Character
    {
        public Ranger(string name)
        {
            Name = name;

            // Init the stats
            basePrimaryAttrubytes.SetAll(1, 7, 1);
            attributes.SetAll(1, 7, 1);
            statGain.SetAll(1, 5, 1);
            attributes.Primary = "Dexterity";

            // Allowed weapon types
            AllowedWeaponTypes.Add(WeaponType.Bow);

            // Allowed armour types
            AllowedArmourTypes.Add(ArmourType.Leather);
            AllowedArmourTypes.Add(ArmourType.Mail);

        }
    }
}
