﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment_1.CharacterRelated
{
    public class Mage : Character
    {

        public Mage(string name)
        {
            Name = name;

            // Init the stats
            basePrimaryAttrubytes.SetAll(1, 1, 8);
            attributes.SetAll(1, 1, 8);
            statGain.SetAll(1, 1, 5);
            attributes.Primary = "Intelligence";

            // Allowed weapon types
            AllowedWeaponTypes.Add(WeaponType.Staff);
            AllowedWeaponTypes.Add(WeaponType.Wand);

            // Allowed armour types
            AllowedArmourTypes.Add(ArmourType.Cloth);
        }
    }
}
