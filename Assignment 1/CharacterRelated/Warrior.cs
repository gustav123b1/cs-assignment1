﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment_1.CharacterRelated
{
    public class Warrior : Character
    {
        public Warrior(string name)
        {
            Name = name;

            // Init the stats
            basePrimaryAttrubytes.SetAll(5, 2, 1);
            attributes.SetAll(5, 2, 1);
            statGain.SetAll(3, 2, 1);
            attributes.Primary = "Strength";

            // Allowed weapon types
            AllowedWeaponTypes.Add(WeaponType.Axe);
            AllowedWeaponTypes.Add(WeaponType.Hammer);
            AllowedWeaponTypes.Add(WeaponType.Sword);

            // Allowed armour types
            AllowedArmourTypes.Add(ArmourType.Mail);
            AllowedArmourTypes.Add(ArmourType.Plate);

        }
    }
}
