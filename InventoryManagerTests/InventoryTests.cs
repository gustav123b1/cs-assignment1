using Assignment_1.InventoryRelated;
using Assignment_1.InventoryRelated.ArmourItem;
using Assignment_1.InventoryRelated.WeaponItem;

namespace InventoryManagerTests
{
    public class InventoryTests
    {
        #region Creation
        [Fact]
        public void Constructor_InitSize_ShouldCreateInventoryFourSlots()
        {
            // Arrange
            int size = 4;
            Inventory inventory = new Inventory();
            int expected = size;
            // Act
            int inventorySize = inventory.GetInventorySize();
            // Assert
            Assert.Equal(expected, inventorySize);
        }
        #endregion

        #region Getting items
        [Fact]
        public void Get_GetItem_ShouldThrowError_WhenGivenIndexOutOfBounds()
        {
            // Arrange
            Inventory inventory = new Inventory();
            // Act and assert
            Assert.Throws<InventoryException>(() => inventory.GetItem(-1));
        }

        [Fact]
        public void Get_GetEmptySlot_ShouldReturnFirstEmptySlot()
        {
            // Arrange
            Inventory inventory = new Inventory();
            int index = 0;
            int expected = index;
            // Act
            int actual = inventory.GetEmptySlot();
            // Assert
            Assert.Equal(expected, actual);
        }

        #endregion

        #region Inserting and removing items

        [Fact]
        public void Add_AddItem_ShouldAddToFirstSlotIfEmpty()
        {
            // Arrange
            Inventory inventory = new Inventory();
            Weapon itemToAdd = new Weapon(WeaponType.Axe) { Level = 10, Slot = "Weapon" };
            Weapon expected = itemToAdd;
            // Act
            int index = 0;
            inventory.AddItem(itemToAdd);
            Item actual = inventory.GetItem(index);
            // Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Add_AddWeapon_ShouldReturnSuccessMessage()
        {
            // Arrange
            Inventory inventory = new Inventory();
            Weapon itemToAdd = new Weapon(WeaponType.Axe) { Level = 10, Slot = "Weapon" };
            string expected = "New weapon equipped!";
            // Act
            string actual = inventory.AddItem(itemToAdd);
            // Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Add_AddArmour_ShouldReturnSuccessMessage()
        {
            // Arrange
            Inventory inventory = new Inventory();
            Armour itemToAdd = new Armour(ArmourType.Cloth) { Level = 10, Slot = "Armour" };
            string expected = "New armour equipped!";
            // Act
            string actual = inventory.AddItem(itemToAdd);
            // Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Add_AddItem_ShouldAddToFirstAvailableEmptySlot()
        {
            // Arrange
            Inventory inventory = new Inventory();
            Armour firstItem = new Armour(ArmourType.Cloth) { Level = 1, Slot = "Armour" };
            Armour itemToAdd = new Armour(ArmourType.Leather) { Level = 2, Slot = "Armour" };
            Armour expected = itemToAdd;
            // Act
            int indexOfItemToAdd = 1;
            inventory.AddItem(firstItem);
            inventory.AddItem(itemToAdd);
            Item actual = inventory.GetItem(indexOfItemToAdd);
            // Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Add_AddItem_ShouldAddToProvidedIndex_IfEmpty()
        {
            // Arrange
            Inventory inventory = new Inventory();
            Weapon itemToAdd = new Weapon(WeaponType.Axe) { Level = 2, Slot = "Weapon" };
            Weapon expected = itemToAdd;
            // Act
            int indexOfItemToAdd = 2;
            inventory.AddItem(itemToAdd, indexOfItemToAdd);
            Item actual = inventory.GetItem(indexOfItemToAdd);
            // Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Add_AddItem_ShouldAddToProvidedIndex_IfItemsAreSameType()
        {
            // Arrange
            Inventory inventory = new Inventory();
            Weapon firstItem = new Weapon(WeaponType.Bow) { Level = 1, Slot = "Weapon" };
            Weapon itemToAdd = new Weapon(WeaponType.Hammer) { Level = 2, Slot = "Weapon" };
            Weapon expected = itemToAdd;
            // Act
            int indexOfItemToAdd = 2;
            inventory.AddItem(firstItem, indexOfItemToAdd);
            inventory.AddItem(itemToAdd, indexOfItemToAdd);
            Item actual = inventory.GetItem(indexOfItemToAdd);
            // Assert
            Assert.Equal(expected, actual);
        }


        [Fact]
        public void Add_AddItem_ShouldAddWeaponToAvailableWeaponSlot_IfNoIndexIsProvided_IfNoWeaponExists()
        {
            // Arrange
            Inventory inventory = new Inventory();
            Armour armour = new Armour(ArmourType.Plate) { Level = 2, Slot = "Armour" };
            Weapon weapon = new Weapon(WeaponType.Wand) { Level = 2, Slot = "Weapon" };
            Weapon expected = weapon;
            int weaponIndex = 1;
            // Act
            inventory.AddItem(armour);
            inventory.AddItem(weapon);
            Item actual = inventory.GetItem(weaponIndex);
            // Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Add_AddItem_ShouldAddWeaponToAvailableWeaponSlot_IfNoIndexIsProvided_IfWeaponExists()
        {
            // Arrange
            Inventory inventory = new Inventory();
            Weapon firstWeapon = new Weapon(WeaponType.Staff) { Level = 2, Slot = "Weapon" };
            Weapon secondWeapon = new Weapon(WeaponType.Dagger) { Level = 2, Slot = "Weapon" };
            Weapon expected = secondWeapon;
            int weaponIndex = 0;
            // Act
            inventory.AddItem(firstWeapon);
            inventory.AddItem(secondWeapon);
            Item actual = inventory.GetItem(weaponIndex);
            // Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Add_AddItem_ShouldAddWeaponToAvailableWeaponSlot_IfNoIndexIsProvided_IfWeaponExists_IfArmourExist()
        {
            // Arrange
            Inventory inventory = new Inventory();
            Armour armour = new Armour(ArmourType.Mail) { Level = 1, Slot = "Armour" };
            Weapon firstWeapon = new Weapon(WeaponType.Axe) { Level = 2, Slot = "Weapon" };
            Weapon secondWeapon = new Weapon(WeaponType.Hammer) { Level = 2, Slot = "Weapon" };
            Weapon expected = secondWeapon;
            int weaponIndex = 1;
            // Act
            inventory.AddItem(armour);
            inventory.AddItem(firstWeapon);
            inventory.AddItem(secondWeapon);
            Item actual = inventory.GetItem(weaponIndex);
            // Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Add_AddItem_ShouldThrowError_IfAddingItemToProvidedIndex_IfItemsAreDifferentTypes()
        {
            // Arrange
            Inventory inventory = new Inventory();
            Weapon firstItem = new Weapon(WeaponType.Axe) { Level = 1, Slot = "Weapon" };
            Armour itemToAdd = new Armour(ArmourType.Cloth) { Level = 1, Slot = "Armour" };

            // Act & Assert
            int indexOfItemToAdd = 2;
            inventory.AddItem(firstItem, indexOfItemToAdd);
            Assert.Throws<InventoryException>(() => inventory.AddItem(itemToAdd, indexOfItemToAdd));
        }

        [Fact]
        public void Add_AddItem_ShouldThrowError_IfInventoryIsFull()
        {
            // Arrange
            Inventory inventory = new Inventory();
            Armour headArmour = new Armour(ArmourType.Cloth) { Level = 1, Slot = "Armour" };
            Armour bodyArmour = new Armour(ArmourType.Leather) { Level = 1, Slot = "Armour" };
            Armour legsArmour = new Armour(ArmourType.Mail) { Level = 1, Slot = "Armour" };
            Weapon swordWeapon = new Weapon(WeaponType.Sword) { Level = 2, Slot = "Weapon" };
            Armour outOfBoundsArmour = new Armour(ArmourType.Plate) { Level = 1, Slot = "Armour" };

            // Act & Assert
            inventory.AddItem(headArmour);
            inventory.AddItem(bodyArmour);
            inventory.AddItem(legsArmour);
            inventory.AddItem(swordWeapon);
            Assert.Throws<InventoryException>(() => inventory.AddItem(outOfBoundsArmour));
        }

        [Fact]
        public void Add_AddItem_ShouldThrowError_IfGivenIndexOutOfBounds()
        {
            // Arrange
            Inventory inventory = new Inventory();
            Armour armour = new Armour(ArmourType.Plate) { Level = 1, Slot = "Armour" };
            int outOfBoundsIndex = 10;

            // Act & Assert
            Assert.Throws<KeyNotFoundException>(() => inventory.AddItem(armour, outOfBoundsIndex));
        }
        #endregion
    }
}