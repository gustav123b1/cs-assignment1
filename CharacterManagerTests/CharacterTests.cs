using Assignment_1;
using Assignment_1.CharacterRelated;
using Assignment_1.InventoryRelated;
using Assignment_1.InventoryRelated.ArmourItem;
using Assignment_1.InventoryRelated.WeaponItem;

namespace CharacterManagerTests
{
    public class CharacterTests
    {
        #region Create
        [Fact]
        public void CreatingCharacter_ShouldGetInitAttributes()
        {
            // Arrange
            string name = "Name of Mage";
            Mage mage = new Mage(name);

            // Act
            // A Mage begins at level 1 with:
            PrimaryAttributes expectedAttributes = new PrimaryAttributes()
            {
                Strength = 1,
                Dexterity = 1,
                Intelligence = 8,
            };

            int[] expected = expectedAttributes.GetAll();
            int[] actual = mage.attributes.GetAll();
            // Assert
            Assert.Equal(expected, actual);
        }
        #endregion

        #region Level up
        [Fact]
        public void LevelingUp_ShouldIncreaseAttributes()
        {
            // Arrange
            string name = "Name of Mage";
            Mage mage = new Mage(name);
            mage.LevelUp();

            // Mage init attributes: 1, 1, 8
            // Mage level up attribute increase: 1, 1, 5
            // Mage level 2: 1+1, 1+1, 8+5 = 2, 2, 13
            PrimaryAttributes expectedAttributes = new PrimaryAttributes()
            {
                Strength = 2,
                Dexterity = 2,
                Intelligence = 13,
            };
            int[] expected = expectedAttributes.GetAll();
            int[] actual = mage.attributes.GetAll();
            // Act
            Assert.Equal(expected, actual);
        }
        #endregion

        #region Inventory

        #region Add Items
        [Fact]
        public void AddingItem_Armour_WithCorrespondingAttributes_WithLowerOrSameLevel_ShouldWork()
        {
            // Arrange
            string name = "Name of Mage";
            Mage mage = new Mage(name);
            Armour testClothHead = new Armour(ArmourType.Cloth) { Level = 1, Slot = "Head", Attributes = new PrimaryAttributes() { Intelligence = 5 } };
            Armour expected = testClothHead;
            // Act
            mage.AddItemToInventory(testClothHead);
            Item actual = mage.GetInventoryItem(0);
            // Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void AddingItem_Armour_WithCorrespondingAttributes_WithTooHighLevel_InvalidArmorException()
        {
            // Arrange
            string name = "Name of Mage";
            Mage mage = new Mage(name);
            Armour testClothHead = new Armour(ArmourType.Cloth) { Level = 10, Slot = "Head", Attributes = new PrimaryAttributes() { Intelligence = 5 } };

            // Act & Assert
            Assert.Throws<InvalidArmorException>(() => mage.AddItemToInventory(testClothHead));
        }

        [Fact]
        public void AddingItem_Weapon_WithCorrespondingAttributes_WithTooHighLevel_InvalidWeaponException()
        {
            // Arrange
            string name = "Name of Mage";
            Mage mage = new Mage(name);
            Weapon testStaff = new Weapon(WeaponType.Staff) { Level = 10 };

            // Act & Assert
            Assert.Throws<InvalidWeaponException>(() => mage.AddItemToInventory(testStaff));
        }

        [Fact]
        public void AddingItem_Armour_WithWrongAttributes_InvalidArmorException()
        {
            // Arrange
            string name = "Name of Mage";
            Mage mage = new Mage(name);
            Armour testClothHead = new Armour(ArmourType.Leather) { Level = 1, Slot = "Head", Attributes = new PrimaryAttributes() { Intelligence = 5 } };

            // Act & Assert
            Assert.Throws<InvalidArmorException>(() => mage.AddItemToInventory(testClothHead));
        }

        [Fact]
        public void AddingItem_Weapon_WithWrongAttributes_InvalidWeaponException()
        {
            // Arrange
            string name = "Name of Mage";
            Mage mage = new Mage(name);
            Weapon testClothHead = new Weapon(WeaponType.Sword) { Level = 1};

            // Act & Assert
            Assert.Throws<InvalidWeaponException>(() => mage.AddItemToInventory(testClothHead));
        }
        #endregion

        #endregion

        #region Get
        [Fact]
        public void GetTotalAttributes_ShouldReturnTotalAttributes()
        {
            // Arrange
            string name = "Name of Mage";
            Mage mage = new Mage(name);

            // Act
            Armour testClothHead = new Armour(ArmourType.Cloth) { Level = 1, Slot = "Head", Attributes = new PrimaryAttributes() { Intelligence = 2 } };
            mage.AddItemToInventory(testClothHead);

            // A Mage begins at level 1 with: 1, 1, 8
            // Cloth gives 5 int: 1+0, 1+0, 8+2 = 1, 1, 10
            PrimaryAttributes expectedAttributes = new PrimaryAttributes()
            {
                Strength = 1,
                Dexterity = 1,
                Intelligence = 10,
            };
            int[] expected = new int[3] { expectedAttributes.Strength, expectedAttributes.Strength, expectedAttributes.Strength }; ;

            PrimaryAttributes actualAttributes = mage.GetTotalAttributes();
            int[] actual = new int[3] { actualAttributes.Strength, actualAttributes.Strength , actualAttributes.Strength };

            // Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void GetDamage_WithoutWeapon_WithoutArmour()
        {
            // Arrange
            string name = "Name of Mage";
            Mage mage = new Mage(name);

            // Act

            // A Mage begins at level 1 with: 1, 1, 8
            PrimaryAttributes expectedAttributes = new PrimaryAttributes()
            {
                Strength = 1,
                Dexterity = 1,
                Intelligence = 8,
            };
            // No weapon means 1 dps apparently..
            double expectedWeaponDps = 1;
            double expected = expectedWeaponDps * (1 + (expectedAttributes.Intelligence / 100));
            double actual = mage.GetDamage();
            // Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void GetDamage_WithValidWeapon_WithoutArmour()
        {
            // Arrange
            string name = "Name of Mage";
            Mage mage = new Mage(name);
            Weapon wand = new Weapon(WeaponType.Wand)
            {
                Level = 1,
                Slot = "Weapon",
                WeaponAttributes = new WeaponAttributes() { Damage = 5, AttackSpeed = 5 },
                Name = "Wand"
            };
            // Act
            mage.AddItemToInventory(wand);
            // A Mage begins at level 1 with: 1, 1, 8
            PrimaryAttributes expectedAttributes = new PrimaryAttributes()
            {
                Strength = 1,
                Dexterity = 1,
                Intelligence = 8,
            };
            double expectedWeaponDamage = 5;
            double expectedWeaponAttackSpeed = 5;
            double expectedWeaponDps = expectedWeaponDamage * expectedWeaponAttackSpeed;
            double expected = expectedWeaponDps * (1 + (expectedAttributes.Intelligence / 100));

            double actual = mage.GetDamage();
            // Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void GetDamage_WithValidWeapon_WithValidArmour()
        {
            // Arrange
            string name = "Name of Mage";
            Mage mage = new Mage(name);
            Weapon wand = new Weapon(WeaponType.Wand)
            {
                Level = 1,
                Slot = "Weapon",
                WeaponAttributes = new WeaponAttributes() { Damage = 5, AttackSpeed = 5 },
                Name = "Wand"
            };
            Armour cloth = new Armour(ArmourType.Cloth) { Attributes = new PrimaryAttributes() { Intelligence = 5 } };
            // Act
            mage.AddItemToInventory(wand);
            mage.AddItemToInventory(cloth);
            // A Mage begins at level 1 with: 1, 1, 8, Armour gives: 0, 0, 5, Expected -> 1, 1, 13
            PrimaryAttributes expectedAttributes = new PrimaryAttributes()
            {
                Strength = 1,
                Dexterity = 1,
                Intelligence = 13,
            };
            double expectedWeaponDamage = 5;
            double expectedWeaponAttackSpeed = 5;
            double expectedWeaponDps = expectedWeaponDamage * expectedWeaponAttackSpeed;
            double expected = expectedWeaponDps * (1 + (expectedAttributes.Intelligence / 100));

            double actual = mage.GetDamage();
            // Assert
            Assert.Equal(expected, actual);
        }

        #endregion
    }
}